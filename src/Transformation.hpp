#pragma once

#include <vector>

#include "Color.hpp"
#include "Image.hpp"

namespace falcon
{

    class LocalTransformation
    {
    public:
        virtual Color operator()(const Image::Pixel &) = 0;
    };

    class GlobalTransformation
    {
        friend Image;

    public:
        GlobalTransformation() : m_image(0, 0) {}

        virtual Color operator()(unsigned int, unsigned int) = 0;

    protected:
        virtual void setReferenceImage(const Image &image) { m_image = image; }

        Image m_image;
    };

    template <unsigned int N>
    class TransformationSIMD
    {
    public:
        virtual void operator()(Color *) = 0;
    };

    class ConvolutionTransformation : public GlobalTransformation
    {
    public:
        struct Filter
        {
            unsigned int kernel_size;
            std::vector<float> coefficients;
        };

        ConvolutionTransformation(const Filter &);

        virtual Color operator()(unsigned int, unsigned int) override;

    protected:
        virtual void setReferenceImage(const Image &image) override;

    private:
        Filter m_filter;
    };

    class GrayScaleTransformation : public LocalTransformation
    {
    public:
        virtual Color operator()(const Image::Pixel &) override;
    };

    class SepiaTransformation : public LocalTransformation
    {
    public:
        virtual Color operator()(const Image::Pixel &) override;
    };

    class BinaryTransformation : public LocalTransformation
    {
    public:
        BinaryTransformation(float = 0.5f);

        virtual Color operator()(const Image::Pixel &) override;

    private:
        float m_threshold;
    };

    class LuminosityTransformation : public LocalTransformation
    {
    public:
        virtual Color operator()(const Image::Pixel &) override;
    };

    class NegativeTransformation : public LocalTransformation
    {
    public:
        virtual Color operator()(const Image::Pixel &) override;
    };

    template <unsigned int N>
    class NegativeTransformationSIMD : public TransformationSIMD<N>
    {
    public:
        virtual void operator()(Color *) override;
    };

    class SaturationTransformation : public LocalTransformation
    {
    public:
        SaturationTransformation(float);

        virtual Color operator()(const Image::Pixel &) override;
    };

    class ContrastTransformation : public LocalTransformation
    {
    public:
        ContrastTransformation(const std::vector<unsigned int> &);

        virtual Color operator()(const Image::Pixel &) override;

    private:
        std::vector<float> m_cumulative_histogram;
    };

    class GammaCorrectionTransformation : public LocalTransformation
    {
    public:
        GammaCorrectionTransformation(float = 2.2);

        virtual Color operator()(const Image::Pixel &) override;

    private:
        float m_gamma;
    };

    class FlipHorizontallyTransformation : public GlobalTransformation
    {
    public:
        virtual Color operator()(unsigned int x, unsigned int y) override;
    };

    class FlipVerticallyTransformation : public GlobalTransformation
    {
    public:
        virtual Color operator()(unsigned int x, unsigned int y) override;
    };

    class DenoiseTransformation : public GlobalTransformation
    {
    public:
        struct DenoiseInfo
        {
            Image albedo_pass;
            Image normal_pass;
            bool prefilter_albedo = false;
            bool prefilter_normal = false;
            bool hdr = false;
        };

        DenoiseTransformation(const DenoiseInfo &);

        virtual Color operator()(unsigned int x, unsigned int y) override;

    protected:
        void setReferenceImage(const Image &) override;

    private:
        std::vector<float> m_normal_image;
        std::vector<float> m_albedo_image;
        bool m_prefilter_normal;
        bool m_prefilter_albedo;
    };

    class EdgeDetectionTransformation : public ConvolutionTransformation
    {
    public:
        EdgeDetectionTransformation();

        virtual Color operator()(unsigned int x, unsigned int y) override;
    };
}