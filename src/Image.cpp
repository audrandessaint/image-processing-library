#include "Image.hpp"

#include <cstring>
#include <algorithm>
#include <execution>

#include "Computation.hpp"
#include "Transformation.hpp"

using namespace falcon;

Image::Image(unsigned int width, unsigned int height) : m_width(width),
                                                        m_height(height),
                                                        m_opened(false)
{
    m_pixels = new Color[width * height];
    std::fill(m_pixels, m_pixels + width * height, Color());
}

Image::Image(std::filesystem::path path, std::shared_ptr<FileFormat> loader) : m_pixels(nullptr), m_opened(false)
{
    load(path, loader);
}

Image::Image(const Image &other)
{
    *this = other;
}

void Image::operator=(const Image &other)
{
    m_width = other.m_width;
    m_height = other.m_height;
    m_opened = other.m_opened;

    if (m_pixels)
    {
        delete[] m_pixels;
    }

    m_pixels = new Color[other.m_width * other.m_height];
    std::copy(&other.m_pixels[0], &other.m_pixels[other.m_width * other.m_height], m_pixels);
}

Image::~Image()
{
    if (m_pixels)
    {
        delete[] m_pixels;
    }
}

bool Image::load(std::filesystem::path path, std::shared_ptr<FileFormat> loader)
{
    if (m_pixels)
    {
        delete[] m_pixels;
        m_pixels = nullptr;
    }

    if (!loader->load(path))
    {
        m_opened = false;
        return false;
    }

    m_width = loader->getWidth();
    m_height = loader->getHeight();

    m_pixels = new Color[m_width * m_height];
    loader->moveToImageBuffer(m_pixels);

    m_opened = true;
    return true;
}

bool Image::save(std::filesystem::path path, std::shared_ptr<FileFormat> saver) const
{
    if (m_pixels == nullptr || m_width == 0 || m_height == 0)
    {
#ifdef _DEBUG
        std::cerr << "Image::save : attempted to save an empty image" << std::endl;
#endif
        return false;
    }

    saver->setWidth(m_width);
    saver->setHeight(m_height);
    saver->fillBuffer(m_pixels);

    return saver->save(path);
}

void Image::apply(std::shared_ptr<LocalTransformation> transformation)
{
    for (unsigned int i = 0; i < m_width * m_height; i++)
    {
        unsigned int x = i % m_width;
        unsigned int y = i / m_width;
        m_pixels[i] = (*transformation)({x, y, m_pixels[i]});
    }
}

void Image::apply(std::shared_ptr<GlobalTransformation> transformation)
{
    transformation->setReferenceImage(*this);

    for (unsigned int i = 0; i < m_width * m_height; i++)
    {
        unsigned int x = i % m_width;
        unsigned int y = i / m_width;
        m_pixels[i] = (*transformation)(x, y);
    }
}

void Image::execute(std::shared_ptr<LocalComputation> computation)
{
    for (unsigned int i = 0; i < m_width * m_height; i++)
    {
        unsigned int x = i % m_width;
        unsigned int y = i / m_width;
        (*computation)({x, y, m_pixels[i]});
    }
}
