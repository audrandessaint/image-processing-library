#include "../Computation.hpp"

using namespace falcon;

HistogramComputation::HistogramComputation(unsigned int resolution) : m_histogram(resolution, 0)
{
}

HistogramComputation::~HistogramComputation()
{
}

const std::vector<unsigned int> &HistogramComputation::getHistogram() const
{
    return m_histogram;
}

void HistogramComputation::operator()(const Image::Pixel &pixel)
{
    Color color = pixel.color;
    color.toHSV();

    unsigned int n = static_cast<unsigned int>(pixel.color.value() * m_histogram.size());
    m_histogram[n]++;
}