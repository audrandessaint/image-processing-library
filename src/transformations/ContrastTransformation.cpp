#include "../Transformation.hpp"

#include <numeric>

using namespace falcon;

ContrastTransformation::ContrastTransformation(const std::vector<unsigned int> &histogram) : m_cumulative_histogram(histogram.size() + 1, 0)
{
    unsigned int pixel_count = std::reduce(histogram.begin(), histogram.end());

    for (unsigned int i = 1; i < histogram.size() + 1; i++)
    {
        m_cumulative_histogram[i] = m_cumulative_histogram[i - 1] + static_cast<float>(histogram[i - 1]) / pixel_count;
    }
}

Color ContrastTransformation::operator()(const Image::Pixel &pixel)
{
    Color color = pixel.color;

    color.toHSV();
    color.value() = m_cumulative_histogram[color.value() * (m_cumulative_histogram.size() - 1)];
    color.toRGB();

    return color;
}