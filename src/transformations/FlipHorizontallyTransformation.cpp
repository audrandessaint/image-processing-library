#include "../Transformation.hpp"

using namespace falcon;

Color FlipHorizontallyTransformation::operator()(unsigned int x, unsigned int y)
{
    return m_image.getPixel(x, m_image.height() - 1 - y);
}