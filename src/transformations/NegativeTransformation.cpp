#include "../Transformation.hpp"

#include <immintrin.h>

using namespace falcon;

Color NegativeTransformation::operator()(const Image::Pixel &pixel)
{
    return Color(1.f, 1.f, 1.f, 2.f) - pixel.color;
}

#ifdef __AVX512__
template <>
void NegativeTransformationSIMD<16>::operator()(Color *colors)
{
    alignas(32) float red_buffer[16];
    alignas(32) float green_buffer[16];
    alignas(32) float blue_buffer[16];
    alignas(32) float alpha_buffer[16];

    for (unsigned int i = 0; i < 16; i++)
    {
        red_buffer[i] = colors[i].red();
        green_buffer[i] = colors[i].green();
        blue_buffer[i] = colors[i].blue();
        alpha_buffer[i] = colors[i].alpha();
    }

    __m512 red = _mm512_load_ps(red_buffer);
    __m512 green = _mm512_load_ps(green_buffer);
    __m512 blue = _mm512_load_ps(blue_buffer);
    __m512 alpha = _mm512_load_ps(alpha_buffer);

    red = _mm512_sub_ps(_mm512_set1_ps(1), red);
    green = _mm512_sub_ps(_mm512_set1_ps(1), green);
    blue = _mm512_sub_ps(_mm512_set1_ps(1), blue);
    alpha = _mm512_sub_ps(_mm512_set1_ps(1), alpha);

    // float *buffer = (float *)aligned_alloc(32, 16 * sizeof(float));
    //_mm512_store_ps(buffer, _mm512_set1_ps(1.f));

    red = _mm512_set1_ps(1.f);
    green = _mm512_set1_ps(1.f);
    blue = _mm512_set1_ps(1.f);
    alpha = _mm512_set1_ps(1.f);

    // std::cout << "n1.3" << std::endl;

    _mm512_store_ps(red_buffer, red);
    _mm512_store_ps(green_buffer, green);
    _mm512_store_ps(blue_buffer, blue);
    _mm512_store_ps(alpha_buffer, alpha);
    // std::cout << "n2" << std::endl;

    for (unsigned int i = 0; i < 16; i++)
    {
        colors[i].red() = red_buffer[i];
        colors[i].green() = green_buffer[i];
        colors[i].blue() = blue_buffer[i];
        colors[i].alpha() = alpha_buffer[i];
    }
}
#endif

// template void NegativeTransformationSIMD<16>::operator()(Color *colors);