#include "../Transformation.hpp"

using namespace falcon;

Color GrayScaleTransformation::operator()(const Image::Pixel &pixel)
{
    return pixel.color.gray();
}