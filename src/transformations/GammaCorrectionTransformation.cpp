#include "../Transformation.hpp"

using namespace falcon;

GammaCorrectionTransformation::GammaCorrectionTransformation(float gamma) : m_gamma(gamma)
{
}

Color GammaCorrectionTransformation::operator()(const Image::Pixel &pixel)
{
    Color color = pixel.color;
    color.gammaCorrection(m_gamma);
    return color;
}