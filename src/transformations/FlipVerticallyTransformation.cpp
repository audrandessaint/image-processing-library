#include "../Transformation.hpp"

using namespace falcon;

Color FlipVerticallyTransformation::operator()(unsigned int x, unsigned int y)
{
    return m_image.getPixel(m_image.width() - 1 - x, y);
}