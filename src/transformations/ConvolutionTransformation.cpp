#include "../Transformation.hpp"

#include <cstring>

#include <fftw3.h>

using namespace falcon;

#include <iostream>
void convolve(fftwf_complex *kernel_out, fftwf_complex *image_out, fftwf_complex *product, float *result, fftwf_plan forward_plan, fftwf_plan backward_plan, unsigned int width, unsigned int height)
{
    fftwf_execute(forward_plan);
    for (unsigned int i = 0; i < width * (height / 2 + 1); i++)
    {
        product[i][0] = kernel_out[i][0] * image_out[i][0] - kernel_out[i][1] * image_out[i][1];
        product[i][1] = kernel_out[i][0] * image_out[i][1] + kernel_out[i][1] * image_out[i][0];
    }
    fftwf_execute(backward_plan);
    for (unsigned int i = 0; i < width * height; i++)
    {
        result[i] /= width * height;
    }
}

ConvolutionTransformation::ConvolutionTransformation(const Filter &filter) : m_filter(filter)
{
}

void ConvolutionTransformation::setReferenceImage(const Image &image)
{
    GlobalTransformation::setReferenceImage(image);

    unsigned int width = m_image.width();
    unsigned int height = m_image.height();

    float *kernel_in = fftwf_alloc_real(width * height);
    float *image_in = fftwf_alloc_real(width * height);
    float *result = fftwf_alloc_real(width * height);

    fftwf_complex *kernel_out = fftwf_alloc_complex(width * (height / 2 + 1));
    fftwf_complex *image_out = fftwf_alloc_complex(width * (height / 2 + 1));
    fftwf_complex *product = fftwf_alloc_complex(width * (height / 2 + 1));

    fftwf_plan kernel_plan = fftwf_plan_dft_r2c_2d(width, height, kernel_in, kernel_out, FFTW_ESTIMATE);
    fftwf_plan forward_plan = fftwf_plan_dft_r2c_2d(width, height, image_in, image_out, FFTW_ESTIMATE);
    fftwf_plan backward_plan = fftwf_plan_dft_c2r_2d(width, height, product, result, FFTW_ESTIMATE);

    std::memset(kernel_in, 0, width * height * sizeof(float));

    unsigned int split_position = (m_filter.kernel_size - 1) / 2;

    for (unsigned int i = 0; i < split_position; i++)
    {
        for (unsigned int j = 0; j < split_position; j++)
        {
            std::cout << i << ", " << i << std::endl;
            unsigned int index = (width - 1 - split_position + i) + (height - 1 - split_position + j) * width;
            kernel_in[index] = m_filter.coefficients[i + j * m_filter.kernel_size];
        }
    }

    for (unsigned int i = 0; i < split_position; i++)
    {
        for (unsigned int j = split_position; j < m_filter.kernel_size; j++)
        {
            unsigned int index = (width - 1 - split_position + i) + (j - split_position) * width;
            kernel_in[index] = m_filter.coefficients[i + j * m_filter.kernel_size];
        }
    }

    for (unsigned int i = split_position; i < m_filter.kernel_size; i++)
    {
        for (unsigned int j = 0; j < split_position; j++)
        {
            unsigned int index = (i - split_position) + (height - 1 - split_position + j) * width;
            kernel_in[index] = m_filter.coefficients[i + j * m_filter.kernel_size];
        }
    }

    for (unsigned int i = split_position; i < m_filter.kernel_size; i++)
    {
        for (unsigned int j = split_position; j < m_filter.kernel_size; j++)
        {
            unsigned int index = (i - split_position) + (j - split_position) * width;
            kernel_in[index] = m_filter.coefficients[i + j * m_filter.kernel_size];
        }
    }

    fftwf_execute(kernel_plan);

    Color *pixels = m_image.pixels();
    for (unsigned int i = 0; i < width * height; i++)
    {
        image_in[i] = pixels[i].red();
    }
    convolve(kernel_out, image_out, product, result, forward_plan, backward_plan, width, height);
    for (unsigned int i = 0; i < width * height; i++)
    {
        pixels[i].red() = result[i];
    }

    for (unsigned int i = 0; i < width * height; i++)
    {
        image_in[i] = pixels[i].green();
    }
    convolve(kernel_out, image_out, product, result, forward_plan, backward_plan, width, height);
    for (unsigned int i = 0; i < width * height; i++)
    {
        pixels[i].green() = result[i];
    }

    for (unsigned int i = 0; i < width * height; i++)
    {
        image_in[i] = pixels[i].blue();
    }
    convolve(kernel_out, image_out, product, result, forward_plan, backward_plan, width, height);
    for (unsigned int i = 0; i < width * height; i++)
    {
        pixels[i].blue() = result[i];
    }

    fftwf_destroy_plan(backward_plan);
    fftwf_destroy_plan(forward_plan);
    fftwf_destroy_plan(kernel_plan);
    fftwf_free(product);
    fftwf_free(image_out);
    fftwf_free(kernel_out);
    fftwf_free(result);
    fftwf_free(image_in);
    fftwf_free(kernel_in);
}

Color ConvolutionTransformation::operator()(unsigned int x, unsigned int y)
{
    return m_image.getPixel(x, y);
}