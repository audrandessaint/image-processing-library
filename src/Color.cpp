#include "Color.hpp"

namespace falcon
{

#ifdef _DEBUG
    #include <doctest.h>

    std::ostream &operator<<(std::ostream &stream, const Color &color)
    {
        stream << "(" << static_cast<int>(color.r) << ", " << static_cast<int>(color.g) << ", " << static_cast<int>(color.b) << ", " << static_cast<int>(color.a) << ")";
        return stream;
    }

    TEST_CASE("")
    {
    }
#endif


const Color Color::White = Color(1, 1, 1);
const Color Color::Black = Color(0, 0, 0);
const Color Color::Gray = Color(0.5, 0.5, 0.5);
const Color Color::Red = Color(1, 0, 0);
const Color Color::Green = Color(0, 1, 0);
const Color Color::Blue = Color(0, 0, 1);
const Color Color::Yellow = Color(1, 1, 0);
const Color Color::Transparent = Color(0, 0, 0, 0);

}