#include "../FileFormat.hpp"
// #define OPENEXR_DLL // do I want that?

#include <OpenEXR/ImfArray.h>
#include <OpenEXR/ImfRgbaFile.h>

using namespace falcon;

bool EXRFileFormat::load(std::filesystem::path path)
{
    Imf::Array2D<Imf::Rgba> frame_buffer;

    try
    {
        Imf::RgbaInputFile file(path.string().c_str());

        Imath::Box2i dw = file.dataWindow();
        unsigned int width = dw.max.x - dw.min.x + 1;
        unsigned int height = dw.max.y - dw.min.y + 1;

        frame_buffer.resizeErase(height, width);

        file.setFrameBuffer(&frame_buffer[0][0] - dw.min.x - dw.min.y * width, 1, width);
        file.readPixels(dw.min.y, dw.max.y); // probably have to change that to report progress
                                             // see: https://openexr.com/en/latest/ReadingAndWritingImageFiles.html#reading-an-rgba-image-file-in-chunks
    }
    catch (Iex::BaseExc error)
    {
#ifdef _DEBUG
        std::cerr << "Image::loadEXR : an error occured while reading the file (" << error.what() << ")" << std::endl;
#endif
        return false;
    }

    m_width = frame_buffer.width();
    m_height = frame_buffer.height();

    m_pixel_buffer = std::vector<float>(m_width * m_height * 4);

    for (unsigned int y = 0; y < m_height; y++)
    {
        for (unsigned int x = 0; x < m_width; x++)
        {
            unsigned int i = x + y * m_width;
            Imf::Rgba pixel = frame_buffer[y][x];

            m_pixel_buffer[i * 4 + 0] = pixel.r;
            m_pixel_buffer[i * 4 + 1] = pixel.g;
            m_pixel_buffer[i * 4 + 2] = pixel.b;
            m_pixel_buffer[i * 4 + 3] = pixel.a;
        }
    }

    return true;
}

bool EXRFileFormat::save(std::filesystem::path path) const
{
    std::vector<Imf::Rgba> frame_buffer(m_width * m_height);

    try
    { 
        Imf::RgbaOutputFile file(path.string().c_str(), m_width, m_height, Imf::WRITE_RGBA);
        file.setFrameBuffer(frame_buffer.data(), 1, m_width);

        for (unsigned int j = 0; j < m_height; j++)
        {
            for (unsigned int i = 0; i < m_width; i++)
            {
                unsigned int index = i + j * m_width;
                frame_buffer[index].r = m_pixel_buffer[index * 4 + 0];
                frame_buffer[index].g = m_pixel_buffer[index * 4 + 1];
                frame_buffer[index].b = m_pixel_buffer[index * 4 + 2];
                frame_buffer[index].a = m_pixel_buffer[index * 4 + 3];
            }

            file.writePixels();

            // progress_callback(static_cast<float>(j + 1) / m_height);
        }
    }
    catch (Iex::BaseExc error)
    {
#ifdef _DEBUG
        std::cerr << "Image::saveEXR : an error occured while writting the file (" << error.what() << ")" << std::endl;
#endif
        return false;
    }

    return true;
}