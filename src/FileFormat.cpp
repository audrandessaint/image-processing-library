#include "FileFormat.hpp"
#include "Color.hpp"

using namespace falcon;

FileFormat::FileFormat() : m_width(0),
                           m_height(0),
                           m_progress(0)
{
}

FileFormat::~FileFormat()
{
}

void FileFormat::moveToImageBuffer(Color *image_buffer)
{
    for (unsigned int i = 0; i < m_width * m_height; i++)
    {
        float r = m_pixel_buffer[i * 4 + 0];
        float g = m_pixel_buffer[i * 4 + 1];
        float b = m_pixel_buffer[i * 4 + 2];
        float a = m_pixel_buffer[i * 4 + 3];

        image_buffer[i] = Color(r, g, b, a);
    }

    m_pixel_buffer.clear();
}

void FileFormat::fillBuffer(const Color *image_buffer)
{
    m_pixel_buffer = std::vector<float>(m_width * m_height * 4);

    for (unsigned int i = 0; i < m_width * m_height; i++)
    {
        m_pixel_buffer[i * 4 + 0] = image_buffer[i].red();
        m_pixel_buffer[i * 4 + 1] = image_buffer[i].green();
        m_pixel_buffer[i * 4 + 2] = image_buffer[i].blue();
        m_pixel_buffer[i * 4 + 3] = image_buffer[i].alpha();
    }
}