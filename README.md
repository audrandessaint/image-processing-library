# Image Processing Library

This is a basic C++20 image processing library developped by Audran Dessaint. It provides an agnostic `Image` class able to load, save and process digital images.

## Features

- Load and save images in various formats (PPM, BMP, PNG, EXR)
- Resize, crop, rotate images
- Apply predified filters (edge detection, blur, gamma correction, sepia, black and white)
- Apply histogram equalization
- Denoise raytraced images using [Intel Open Image Denoise](https://www.openimagedenoise.org/)
- Extension API to support new image formats and image transformations
- support for SIMD operations

## Getting started 

Read the documentation from `docs/index.md`.
